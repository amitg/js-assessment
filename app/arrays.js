exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
	return arr.indexOf(item)
  },

  sum : function(arr) {
	var sum=0;
	for (var i=0;i<arr.length;i++) {
		sum=sum+arr[i];
	}
	return sum
  },

  remove : function(arr, item) {
	while(arr.indexOf(item)>-1){
		arr.splice(arr.indexOf(item),1)
	}
	return arr
  },

  removeWithoutCopy : function(arr, item) {
	while(arr.indexOf(item)>-1){
		arr.splice(arr.indexOf(item),1)
	}
	return arr
  },

  append : function(arr, item) {
	arr.push(item)
	return arr
  },

  truncate : function(arr) {
	return arr.slice(0,arr.length-1)
  },

  prepend : function(arr, item) {
	arr.unshift(item)
	return arr
  },

  curtail : function(arr) {
	arr.shift()
	return arr
  },

  concat : function(arr1, arr2) {
	arr1.concat(arr2)
	return arr1
  },

  insert : function(arr, item, index) {
	var arrLength=arr.length
	for(var i=arrLength;i>index+1;i--){
		arr[i]=arr[i-1]
	}
	arr[index]=item
	return arr
		
  },

  count : function(arr, item) {

  },

  duplicates : function(arr) {

  },

  square : function(arr) {

  },

  findAllOccurrences : function(arr, target) {
/*	var positions=Array();
	var found=false;
	while(found==false){
		var index=arr.indexOf(target)
		if(index==-1)
			found=true
		else
			positions.push(index)
	}*/
  }
};
